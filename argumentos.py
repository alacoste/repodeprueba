from sys import argv
from factorial import factorial


def suma(a, b):
    return a + b


if __name__ == "__main__":
    try:
        if argv[1] == "factorial":
            print(factorial(int(argv[2])))
        elif argv[1] == "suma":
            print(suma(int(argv[2]), int(argv[3])))
    except ValueError:
        print("Monstruo, escribe bien los argumentos")

