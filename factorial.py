#!/usr/bin/python3

def factorial(n):
    result = 1
    for i in range(n, 1, -1):
        result *= i
    return result

# MAIN en python
if __name__ == "__main__":
    for i in range(1, 11):
        print(factorial(i))